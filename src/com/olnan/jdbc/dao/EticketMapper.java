package com.olnan.jdbc.dao;

import com.olnan.jdbc.domain.ETicketDo;

/**
 * @ClassName : EticketDao
 * @Author : ALVIN
 * @Date: 2021/1/23 19:26
 * @Description : 电子票的Dao层接口
 */
public interface EticketMapper<EticketDo> {
    //增加一个电子票
    int insertOne(ETicketDo eticket);
    //删除一个电子票
    int deleteOne(ETicketDo eticket);
    //更新一个
    int updateOne(ETicketDo eticket);
    //通过订单编号查询订单附属信息
    EticketDo selectOneByOrdrId(String ordr_id);
    //是否可用的订单列表
    EticketDo[] selectListByIsAvailable(Boolean is_available);
    //是否已用的订单列表
    EticketDo[] selectListByIsUsed(Boolean is_used);
}
