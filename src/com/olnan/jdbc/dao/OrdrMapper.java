package com.olnan.jdbc.dao;
import com.olnan.jdbc.domain.OrdrDo;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * @ClassName : OrderMapper
 * @Author : Lc
 * @Date: 2021/1/25 11:17
 * @Description : 订单的Dao层接口
 */

public interface OrdrMapper<OrdrDo> {

    //添加一个订单信息
    int insertOne(OrdrDo ordr);
    //删除一个订单
    int deleteOne(String ordr_id);
    //更新一个（根据订单号更新其他属性，可以参考user的mapper）
    int updateOne(OrdrDo order);
    //通过订单编号查询订单信息
    OrdrDo selectOneByOrderId(String ordr_id);
    //通过账号查询某人的订单
    //OrdrDo[] selectOneByAcc(BigInteger acc);
    ArrayList<OrdrDo> selectListByAcc(BigInteger acc);
    //查找某种门票的订单列表
    //OrdrDo[] selectOneByTktId(String tkt_id);
    ArrayList<OrdrDo> selectListByTktId(String tkt_id);
    //查找未支付/已支付的订单
    //OrdrDo[] selectOneByIsPay(Boolean is_pay);
    ArrayList<OrdrDo> selectListByIsPay(Boolean is_pay);
    //查找正在退款状态下的订单
    //OrdrDo[] selectOneByIsReqarfnd(Boolean is_reqarfnd);
    ArrayList<OrdrDo> selectListByIsReqarfnd(Boolean is_reqarfnd);

}
