package com.olnan.jdbc.dao;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * @ClassName : OrganizTypeMatchMapper
 * @Author : Lc
 * @Date: 2021/1/25 12:00
 * @Description : 组织与组织类型编号匹配信息的Dao层接口
 */



public interface OrganizTypeMatchMapper<OrganizTypeMatchDo> {

    //添加
    int insertOne(OrganizTypeMatchDo organiztypematch);
    //删除一个
    int deleteOne(@Param("organization_id") String organization_id, @Param("organizationtype_id") BigInteger organizationtype_id);
    //删除某个类型的所有匹配信息
    int deleteListByOrganizationId(String organization_id);
    //删除某个组织的所有匹配信息
    int deleteListByOrganizationtypeId(BigInteger organizationtype_id);
    //更新一个
    int updateOne(OrganizTypeMatchDo organiztypematch);
    //通过组织类型编号和组织编号查询组织类型匹配附属信息
    ArrayList<OrganizTypeMatchDo> selectListByOrganizationId(String organization_id, BigInteger organizationtype_id);
    ArrayList<OrganizTypeMatchMapper> selectListByOrganizationtypeId(BigInteger organizationtype_id);


}
