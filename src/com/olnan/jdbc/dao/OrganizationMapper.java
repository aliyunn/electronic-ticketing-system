package com.olnan.jdbc.dao;
import com.olnan.jdbc.domain.OrganizationDo;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;

/**
 * @ClassName : OrganizationMapper
 * @Author : Lc
 * @Date: 2021/1/25 11:40
 * @Description : 组织的Dao层接口
 */

public interface OrganizationMapper<OrganizationDo> {

    //添加一个组织
    int insertOne(OrganizationDo organization);
    //删除一个
    int deleteOne(String id);
    //更新一个组织（注意同订单mapper）
    int updateOne(OrganizationDo organization);
    //通过组织编号查询组织附属信息
    OrganizationDo selectOneById(String id);
    //通过组织姓名查找组织
    ArrayList<OrganizationDo> selectListByName(String name);
    //析取某省份组织
    ArrayList<OrganizationDo> selectListByPrvn(String prvn);
    //析取某县区组织
    ArrayList<OrganizationDo> selectListByCountie(String countie);
    //析取某城市组织
    ArrayList<OrganizationDo> selectListByCity(String city);

}
