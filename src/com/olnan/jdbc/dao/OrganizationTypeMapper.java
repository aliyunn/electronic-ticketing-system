package com.olnan.jdbc.dao;
import com.olnan.jdbc.domain.OrganizationTypeDo;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;

/**
 * @ClassName : OrganizationTypeMapper
 * @Author : Lc
 * @Date: 2021/1/25 11:49
 * @Description : OrganizationType的Dao层接口
 */


public interface OrganizationTypeMapper<OrganizationTypeDo> {

    //添加一种类型
    int insertOne(OrganizationTypeDo organizationtype);
    //删除一个类型
    int deleteOne(BigInteger organizationtype);
    //更新一个类型名（注意）
    int updateOne(OrganizationTypeDo organizationtype);
    //通过组织类型编号查询组织类型信息
    OrganizationTypeDo selectOneByOrganizationTypeId(String organizationtype_id);

}
