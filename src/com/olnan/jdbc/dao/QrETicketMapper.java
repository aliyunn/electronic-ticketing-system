package com.olnan.jdbc.dao;

public interface QrETicketMapper<QrETicketDo> {
    //生成二维码信息
    int insertOne(QrETicketDo qreticket);
    //删除一条二维码信息
    int deleteOne(String ordr_id);
    //更改一个二维码信息
    int updateOne(QrETicketDo qreticket);
    //查找一个二维码信息
    QrETicketDo selectOneByOrdrId(String ordr_id);
}
