package com.olnan.jdbc.dao;

public interface RefundOrderInfMapper<RefundOrderInfDo> {
    //增加一条退款订单
    int insertOne(RefundOrderInfDo refundOrderInfDo);
    //删除一条退款订单
    int deleteOne(String ordr_id);
    //更新一条订单的信息
    int updateOne(RefundOrderInfDo refundOrderInfDo);
    //查找一条订单的信息通过订单号
    RefundOrderInfDo selectOneByOrdrId(String ordr_id);
    //查找已成功退款的订单
    RefundOrderInfDo[] selectListByIsRfindSuc(boolean is_rfind_suc);
}
