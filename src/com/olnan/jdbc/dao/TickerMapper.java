package com.olnan.jdbc.dao;

public interface TickerMapper<TickerDo> {
    //增加一条票价信息
    int insertOne(TickerDo ticket);
    //删除一条票价信息
    int deleteOne(String tkt_id);
    //更新一条票价信息
    int updateOne(TickerDo ticket);
    //搜索一条票价信息
    TickerDo selectOneByTktId(String tkt_id);
}
