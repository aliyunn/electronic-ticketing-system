package com.olnan.jdbc.dao;

import java.math.BigInteger;
import java.util.Date;

public interface TicketCheckInfMapper<TicketCheckInfDo> {
    //����һ����Ʊ��Ϣ
    int insertOne(TicketCheckInfDo chticket);
    //ɾ��һ����Ʊ��¼
    int deleteOne(String ordr_id);
    //����һ����Ʊ��¼�����ã�
    int updateOne(TicketCheckInfDo chticket);
    //������Ʊ��¼
    TicketCheckInfDo selectOneByOrdrId(String ordr_id);
    //����ĳ��ƱԱ��Ʊ����
    TicketCheckInfDo[] selectOneByAcc(BigInteger acc);
    //����ĳ��ʧ�ܼ�Ʊ��Ϣ
    TicketCheckInfDo[] selectOneByTurnstileId(boolean turnstile_id);
    //����ĳ�ռ�Ʊ��
    TicketCheckInfDo[] selectOneByCreateTime(Date create_time);
}
