package com.olnan.jdbc.dao;

import java.math.BigInteger;

public interface TicketInfMapper<TicketInfDo> {
    //增加一条组织票务信息
    int insertOne(TicketInfDo ticketinf);
    //删除一条组织票务信息
    int deleteOne(BigInteger organization_id);
    //删除一个组织所有的票务信息
    int deleteListByOrgid(BigInteger organization_id);
    //更新一条组织票务信息
    int updateOne(TicketInfDo ticketinf);
    //搜索一个组织的票务列表
    TicketInfDo[] selectListByOrgId(BigInteger organization_id);
    //检索某票是某个组织卖的（重要）
    TicketInfDo selectOneByTktId(String tkt_id);
}
