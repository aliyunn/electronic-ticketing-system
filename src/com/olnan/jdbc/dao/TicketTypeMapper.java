package com.olnan.jdbc.dao;

import com.olnan.jdbc.domain.TicketTypeDo;
import com.olnan.jdbc.domain.UserDo;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;

/**
 * @ClassName : UserDao
 * @Author : ALVIN
 * @Date: 2021/1/23 19:52
 * @Description : 票类型Dao层接口
 */
public interface TicketTypeMapper {
    int insertOne(TicketTypeDo ticketTypeDo);
    int deleteOne(@Param("tkttyp_id") BigInteger tkttyp_id, @Param("tkt_id") String tkt_id);
    //暂时不用更新
//    int updateOne(TicketTypeDo ticketTypeDo);
    //通过票类型查询有哪些票
    TicketTypeDo[] selectListByTkttypId(BigInteger tkttyp_id);
    //通过票编号查找种类
    TicketTypeDo[] selectListByTktId(String tkt_id);
}
