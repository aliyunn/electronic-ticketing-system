package com.olnan.jdbc.dao;

import com.olnan.jdbc.domain.TicketTypeDo;
import com.olnan.jdbc.domain.TicketTypeMatchDo;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;

/**
 * @ClassName : UserDao
 * @Author : ALVIN
 * @Date: 2021/1/25 19:52
 * @Description : 票类型名与票种类编号相应Dao层接口
 */
public interface TicketTypeMatchMapper {
    int insertOne(TicketTypeMatchDo ticketTypeMatchDo);
    int deleteOne(BigInteger tkttyp_id);
    //更新
    int updateOne(TicketTypeMatchDo ticketTypeMatchDo);
    //通过票编号查询种类名
    TicketTypeMatchDo selectOneByTkttypId(BigInteger tkttyp_id);
    //通过票种类名称查找种类编号
    TicketTypeMatchDo selectOneByTkttypName(String tkttyp_name);
}
