package com.olnan.jdbc.dao;

import com.olnan.jdbc.domain.UserDo;

import java.math.BigInteger;

/**
 * @ClassName : UserDao
 * @Author : ALVIN
 * @Date: 2021/1/23 19:52
 * @Description : 用户Dao层接口
 */
public interface UserMapper {
    int insertOne(UserDo User);
    int deleteOne(BigInteger acc);
    int updateOne(UserDo User);
    //通过账号查询用户信息
    UserDo selectOneByAcc(BigInteger acc);
    //通过姓名查找用户
    UserDo selectOneByName(String name);
    //通过权限查找用户列
    UserDo[] selectListByAuthorityId(String authority_id);
}
