package com.olnan.jdbc.dao;

import com.olnan.jdbc.domain.UserOrderListDo;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;

/**
 * @ClassName : UserDao
 * @Author : ALVIN
 * @Date: 2021/1/25 19:52
 * @Description : 用户订单列表相应Dao层接口
 */
public interface UserOrderListMapper {
    int insertOne(UserOrderListDo userOrderListDo);
    int deleteOne(@Param("acc") BigInteger acc, @Param("ordr_id") String ordr_id);
    //删除某个账号所有订单
    int deleteListByAcc(BigInteger acc);
    //暂时不用更新
//    int updateOne();
    //通过某用户账号查询订单列表
    UserOrderListDo[] selectListByAcc(BigInteger acc);
}
