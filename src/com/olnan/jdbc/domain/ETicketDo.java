package com.olnan.jdbc.domain;

import java.util.Date;

/**
 * @ClassName : ETicketDo
 * @Author : Lc
 * @Date: 2021/1/22 16:56
 * @Description : 这是电子票的数据对象类，用于从数据库得到数据传向上层dao
 */

public class ETicketDo{

    private String ordr_id;
    private Boolean is_available;
    private Boolean is_used;
    private Date use_time;
    private String descr;
    private String remark;
    private Date tkt_pur_tm;
    private Date deadline;
    private Date create_time;
    private Date update_time;

    public ETicketDo() {
    }

    public String getOrdr_id(){
        return ordr_id;
    }

    public void setOrdr_id(String ordr_id){
        this.ordr_id=ordr_id;
    }

    public Boolean getIs_available(){
        return is_available;
    }

    public void setIs_available(Boolean is_available){
        this.is_available=is_available;
    }

    public Boolean getIs_used(){
        return is_used;
    }

    public void setIs_used(Boolean is_used){
        this.is_used=is_used;
    }

    public Date getUse_time(){
        return use_time;
    }

    public void setUse_time(Date use_time){
        this.use_time=use_time;
    }

    public String getDescr(){
        return descr;
    }

    public void setDescr(String descr){
        this.descr=descr;
    }

    public String getRemark(){
        return remark;
    }

    public void setRemark(String remark){
        this.remark=remark;
    }

    public Date getTkt_pur_tm(){
        return tkt_pur_tm;
    }

    public void setTkt_pur_tm(Date tkt_pur_tm){
        this.tkt_pur_tm=tkt_pur_tm;
    }

    public Date getDeadline(){
        return deadline;
    }

    public void setDeadline(Date deadline){
        this.deadline=deadline;
    }

    public Date getCreate_time(){
        return create_time;
    }

    public void setCreate_time(Date create_time){
        this.create_time=create_time;
    }

    public Date getUpdate_time(){
        return update_time;
    }

    public void setUpdate_time(Date update_time){
        this.update_time=update_time;
    }

    @Override
    public String toString() {
        return "ETicketDo{" +
                "ordr_id='" + ordr_id + '\'' +
                ", is_available=" + is_available +
                ", is_used=" + is_used +
                ", use_time=" + use_time +
                ", descr='" + descr + '\'' +
                ", remark='" + remark + '\'' +
                ", tkt_pur_tm=" + tkt_pur_tm +
                ", deadline=" + deadline +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}