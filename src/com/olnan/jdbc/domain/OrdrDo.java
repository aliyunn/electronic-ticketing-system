package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * @ClassName : OrderDo
 * @Author : Lc
 * @Date: 2021/1/22 17:16
 * @Description : 这是订单的数据对象类，用于从数据库得到数据传向上层dao
 */

public class OrdrDo {

    private BigInteger acc;
    private String ordr_id;
    private String tkt_id;
    private Boolean is_pay;
    private DecimalFormat paid;
    private Boolean is_reqarfnd;
    private Date create_time;
    private Date update_time;

    public OrdrDo() {
    }

    public BigInteger getAcc(){
        return acc;
    }

    public void setAcc(BigInteger acc){
        this.acc=acc;
    }

    public String getOrdr_id(){
        return ordr_id;
    }

    public void setOrdr_id(String ordr_id){
        this.ordr_id=ordr_id;
    }

    public String getTkt_id(){
        return tkt_id;
    }

    public void setTkt_id(String tkt_id){
        this.tkt_id=tkt_id;
    }

    public Boolean getIs_pay(){
        return is_pay;
    }

    public void setIs_pay(Boolean is_pay){
        this.is_pay=is_pay;
    }

    public DecimalFormat getPaid(){
        return paid;
    }

    public void setPaid(DecimalFormat paid){
        this.paid=paid;
    }

    public Boolean getIs_reqarfnd(){
        return is_reqarfnd;
    }

    public void setIs_reqarfnd(Boolean is_reqarfnd){
        this.is_reqarfnd=is_reqarfnd;
    }

    public Date getCreate_time(){
        return create_time;
    }

    public void setCreate_time(Date create_time){
        this.create_time=create_time;
    }

    public Date getUpdate_time(){
        return update_time;
    }

    public void setUpdate_time(Date update_time){
        this.update_time=update_time;
    }

    @Override
    public String toString() {
        return "OrderDo{" +
                "acc=" + acc +
                ", ordr_id='" + ordr_id + '\'' +
                ", tkt_id='" + tkt_id + '\'' +
                ", is_pay=" + is_pay +
                ", paid=" + paid +
                ", is_reqarfnd=" + is_reqarfnd +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
