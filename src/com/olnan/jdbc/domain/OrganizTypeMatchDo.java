package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : OrganizationTypeDo
 * @Author : Lc
 * @Date: 2021/1/22 17:51
 * @Description : 这是组织类型id和组织id的数据对象类，用于从数据库得到数据传向上层dao
 */

public class OrganizTypeMatchDo {

    private BigInteger organizationtype_id;
    private String organization_id;
    private Date create_time;
    private Date update_time;

    public OrganizTypeMatchDo() {
    }

    public BigInteger getOrganizationtype_id(){
        return organizationtype_id;
    }

    public void setOrganizationtype_id(BigInteger organizationtype_id){
        this.organizationtype_id=organizationtype_id;
    }

    public String getOrganization_id(){
        return organization_id;
    }

    public void setOrganization_id(String organization_id){
        this.organization_id=organization_id;
    }

    public Date getCreate_time(){
        return create_time;
    }

    public void setCreate_time(Date create_time){
        this.create_time=create_time;
    }

    public Date getUpdate_time(){
        return update_time;
    }

    public void setUpdate_time(Date update_time){
        this.update_time=update_time;
    }

    @Override
    public String toString() {
        return "OrganizTypeMatchDo{" +
                "organizationtype_id=" + organizationtype_id +
                ", organization_id='" + organization_id + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
