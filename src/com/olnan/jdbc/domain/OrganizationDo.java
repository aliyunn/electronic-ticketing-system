package com.olnan.jdbc.domain;

import java.util.Date;

/**
 * @ClassName : OrganizationDo
 * @Author : Lc
 * @Date: 2021/1/22 17:36
 * @Description : 这是组织的数据对象类，用于从数据库得到数据传向上层dao
 */


public class OrganizationDo {

    private String id;
    private String name;
    private String descr;
    private String prvn;
    private String city;
    private String countie;
    private String addr;
    private String contact_inf;
    private Date create_time;
    private Date update_time;

    public OrganizationDo() {
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id=id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getDescr(){
        return descr;
    }

    public void setDescr(String descr){
        this.descr=descr;
    }

    public String getPrvn(){
        return prvn;
    }

    public void setPrvn(String prvn){
        this.prvn=prvn;
    }

    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city=city;
    }

    public String getCountie(){
        return countie;
    }

    public void setCountie(String contact_inf){
        this.countie=contact_inf;
    }

    public String getAddr(){
        return addr;
    }

    public void setAddr(String addr){
        this.addr=addr;
    }

    public String getContact_inf(){
        return  contact_inf;
    }

    public void setContact_inf(String contact_inf){
        this.contact_inf=contact_inf;
    }

    public Date getCreate_time(){
        return create_time;
    }

    public void setCreate_time(Date create_time){
        this.create_time=create_time;
    }

    public Date getUpdate_time(){
        return update_time;
    }

    public void setUpdate_time(Date update_time){
        this.update_time=update_time;
    }

    @Override
    public String toString() {
        return "OrganizationDo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", descr='" + descr + '\'' +
                ", prvn='" + prvn + '\'' +
                ", city='" + city + '\'' +
                ", countie='" + countie + '\'' +
                ", addr='" + addr + '\'' +
                ", contact_inf='" + contact_inf + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
