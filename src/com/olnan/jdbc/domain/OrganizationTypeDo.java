package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : OrganizationTypeDo
 * @Author : Lc
 * @Date: 2021/1/22 17:45
 * @Description : 这是组织类型id和组织类型名称的数据对象类，用于从数据库得到数据传向上层dao
 */

public class OrganizationTypeDo {

    private BigInteger organizationtype_id;
    private String organizationtype_name;
    private Date create_time;
    private Date update_time;

    public OrganizationTypeDo() {
    }

    public BigInteger getOrganizationtype_id(){
        return organizationtype_id;
    }

    public void setOrganizationtype_id(BigInteger organizationtype_id){
        this.organizationtype_id=organizationtype_id;
    }

    public String getOrganizationtype_name(){
        return organizationtype_name;
    }

    public void setOrganizationtype_name(String organizationtype_name){
        this.organizationtype_name=organizationtype_name;
    }

    public Date getCreate_time(){
        return create_time;
    }

    public void setCreate_time(Date create_time){
        this.create_time=create_time;
    }

    public Date getUpdate_time(){
        return update_time;
    }

    public void setUpdate_time(Date update_time){
        this.update_time=update_time;
    }

    @Override
    public String toString() {
        return "OrganizationTypeDo{" +
                "organizationtype_id=" + organizationtype_id +
                ", organizationtype_name='" + organizationtype_name + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
