package com.olnan.jdbc.domain;

import java.util.Date;

/**
 * @ClassName : QrETicketDo
 * @Author : ALVIN
 * @Date: 2021/1/22 16:38
 * @Description : 二维码数据对象类，用于上层Dao
 */
public class QrETicketDo {
    private String ordr_id;
    private String tkt_id;
    private Date ordr_createtime;
    private String random_num;
    private Date create_time;
    private Date update_time;

    public QrETicketDo() {
    }

    public String getOrdr_id() {
        return ordr_id;
    }

    public void setOrdr_id(String ordr_id) {
        this.ordr_id = ordr_id;
    }

    public String getTkt_id() {
        return tkt_id;
    }

    public void setTkt_id(String tkt_id) {
        this.tkt_id = tkt_id;
    }

    public Date getOrdr_createtime() {
        return ordr_createtime;
    }

    public void setOrdr_createtime(Date ordr_createtime) {
        this.ordr_createtime = ordr_createtime;
    }

    public String getRandom_num() {
        return random_num;
    }

    public void setRandom_num(String random_num) {
        this.random_num = random_num;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "QrETicketDo{" +
                "ordr_id='" + ordr_id + '\'' +
                ", tkt_id='" + tkt_id + '\'' +
                ", ordr_createtime=" + ordr_createtime +
                ", random_num='" + random_num + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
