package com.olnan.jdbc.domain;

import java.util.Date;

/**
 * @ClassName : RefundOrderInfDo
 * @Author : ALVIN
 * @Date: 2021/1/22 16:41
 * @Description : 退款订单数据对象类，用于上层Dao
 */
public class RefundOrderInfDo {
    private String ordr_id;
    private Date rfnd_time;
    private Date rfnd_suc_tm;
    private boolean is_rfind_suc;
    private String rfnd_feedback;
    private Date create_time;
    private Date update_time;

    public RefundOrderInfDo() {
    }

    public String getOrdr_id() {
        return ordr_id;
    }

    public void setOrdr_id(String ordr_id) {
        this.ordr_id = ordr_id;
    }

    public Date getRfnd_time() {
        return rfnd_time;
    }

    public void setRfnd_time(Date rfnd_time) {
        this.rfnd_time = rfnd_time;
    }

    public Date getRfnd_suc_tm() {
        return rfnd_suc_tm;
    }

    public void setRfnd_suc_tm(Date rfnd_suc_tm) {
        this.rfnd_suc_tm = rfnd_suc_tm;
    }

    public boolean getIs_rfind_suc() {
        return is_rfind_suc;
    }

    public void setIs_rfind_suc(boolean is_rfind_suc) {
        this.is_rfind_suc = is_rfind_suc;
    }

    public String getRfnd_feedback() {
        return rfnd_feedback;
    }

    public void setRfnd_feedback(String rfnd_feedback) {
        this.rfnd_feedback = rfnd_feedback;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "RefundOrderInfDo{" +
                "ordr_id='" + ordr_id + '\'' +
                ", rfnd_time=" + rfnd_time +
                ", rfnd_suc_tm=" + rfnd_suc_tm +
                ", is_rfind_suc=" + is_rfind_suc +
                ", rfnd_feedback='" + rfnd_feedback + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}

