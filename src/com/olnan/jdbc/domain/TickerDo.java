package com.olnan.jdbc.domain;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * @ClassName : TickerDo
 * @Author : ALVIN
 * @Date: 2021/1/22 16:44
 * @Description : 票的数据对象类，用于上层Dao
 */
public class TickerDo {
    private String tkt_id;
    private DecimalFormat original_prc;
    private float disc;
    private int tot;
    private int sold;
    private Date create_time;
    private Date update_time;

    public TickerDo() {
    }

    public String getTkt_id() {
        return tkt_id;
    }

    public void setTkt_id(String tkt_id) {
        this.tkt_id = tkt_id;
    }

    public DecimalFormat getOriginal_prc() {
        return original_prc;
    }

    public void setOriginal_prc(DecimalFormat original_prc) {
        this.original_prc = original_prc;
    }

    public float getDisc() {
        return disc;
    }

    public void setDisc(float disc) {
        this.disc = disc;
    }

    public int getTot() {
        return tot;
    }

    public void setTot(int tot) {
        this.tot = tot;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "TickerDo{" +
                "tkt_id='" + tkt_id + '\'' +
                ", original_prc=" + original_prc +
                ", disc=" + disc +
                ", tot=" + tot +
                ", sold=" + sold +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
