package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : TicketCheckInfDo
 * @Author : ALVIN
 * @Date: 2021/1/22 16:55
 * @Description : 检票数据对象类，用于传输向上层Dao
 */
public class TicketCheckInfDo {
    private String ordr_id;
    private BigInteger acc;
    private int turnstile_id;
    private boolean is_checktkt_suc;
    private String rsn_chtkt_err;
    private Date create_time;
    private Date update_time;

    public TicketCheckInfDo() {
    }

    public String getOrdr_id() {
        return ordr_id;
    }

    public void setOrdr_id(String ordr_id) {
        this.ordr_id = ordr_id;
    }

    public BigInteger getAcc() {
        return acc;
    }

    public void setAcc(BigInteger acc) {
        this.acc = acc;
    }

    public int getTurnstile_id() {
        return turnstile_id;
    }

    public void setTurnstile_id(int turnstile_id) {
        this.turnstile_id = turnstile_id;
    }

    public boolean getIs_checktkt_suc() {
        return is_checktkt_suc;
    }

    public void setIs_checktkt_suc(boolean is_checktkt_suc) {
        this.is_checktkt_suc = is_checktkt_suc;
    }

    public String getRsn_chtkt_err() {
        return rsn_chtkt_err;
    }

    public void setRsn_chtkt_err(String rsn_chtkt_err) {
        this.rsn_chtkt_err = rsn_chtkt_err;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "TicketCheckInfDo{" +
                "ordr_id='" + ordr_id + '\'' +
                ", acc=" + acc +
                ", turnstile_id=" + turnstile_id +
                ", is_checktkt_suc=" + is_checktkt_suc +
                ", rsn_chtkt_err='" + rsn_chtkt_err + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
