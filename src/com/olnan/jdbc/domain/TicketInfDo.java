package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : TicketInfDo
 * @Author : ALVIN
 * @Date: 2021/1/22 17:02
 * @Description : 组织票务数据对象类，用于上层Dao
 */
public class TicketInfDo {
    private BigInteger organization_id;
    private String tkt_id;
    private Date create_time;
    private Date update_time;

    public TicketInfDo() {
    }

    public BigInteger getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(BigInteger organization_id) {
        this.organization_id = organization_id;
    }

    public String getTkt_id() {
        return tkt_id;
    }

    public void setTkt_id(String tkt_id) {
        this.tkt_id = tkt_id;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "TicketInfDo{" +
                "organization_id=" + organization_id +
                ", tkt_id='" + tkt_id + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
