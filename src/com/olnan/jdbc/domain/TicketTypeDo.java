package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : TicketTypeDo
 * @Author : ALVIN
 * @Date: 2021/1/22 14:33
 * @Description : 票类型数据对象类，用于传递给Dao
 */
public class TicketTypeDo {
    private BigInteger tkttyp_id;
    private String tkt_id;
    private Date create_time;
    private Date update_time;

    public TicketTypeDo() {
    }

    public BigInteger getTkttyp_id() {
        return tkttyp_id;
    }
    public void setTkttyp_id(BigInteger tkttyp_id) {
        this.tkttyp_id = tkttyp_id;
    }
    public String getTkt_id() {
        return tkt_id;
    }
    public void setTkt_id(String tkt_id) {
        this.tkt_id = tkt_id;
    }
    public Date getCreate_time() {
        return create_time;
    }
    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }
    public Date getUpdate_time() {
        return update_time;
    }
    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "TicketTypeDo{" +
                "tkttyp_id=" + tkttyp_id +
                ", tkt_id='" + tkt_id + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
