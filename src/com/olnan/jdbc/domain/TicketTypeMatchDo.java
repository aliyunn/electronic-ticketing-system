package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : TicketTypeMatchDo
 * @Author : ALVIN
 * @Date: 2021/1/22 14:38
 * @Description : 票与种类的对应数据对象，用途传输至Dao
 */
public class TicketTypeMatchDo {
    private BigInteger tkttyp_id;
    private String tkttyp_name;
    private Date create_time;
    private Date update_time;

    public TicketTypeMatchDo() {
    }

    public BigInteger getTkttyp_id() {
        return tkttyp_id;
    }
    public void setTkttyp_id(BigInteger tkttyp_id) {
        this.tkttyp_id = tkttyp_id;
    }
    public String getTkttyp_name() {
        return tkttyp_name;
    }
    public void setTkttyp_name(String tkttyp_name) {
        this.tkttyp_name = tkttyp_name;
    }
    public Date getCreate_time() {
        return create_time;
    }
    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }
    public Date getUpdate_time() {
        return update_time;
    }
    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "TicketTypeMatchDo{" +
                "tkttyp_id=" + tkttyp_id +
                ", tkttyp_name='" + tkttyp_name + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}

