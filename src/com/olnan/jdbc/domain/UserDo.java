package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : UserDo
 * @Author : ALVIN
 * @Date: 2021/1/22 13:03
 * @Description : 这是用户表的数据对象类，用于从数据库得到数据传向上层dao
 */
public class UserDo {
    private BigInteger acc;
    private String passwd;
    private String authority_id;
    private String name;
    private String mailbox;
    private String extra_info_id;
    private Date create_time;
    private Date update_time;

    //无参构造器
    public UserDo() {}

    public BigInteger getAcc() {
        return acc;
    }

    public void setAcc(BigInteger acc) {
        this.acc = acc;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getAuthority_id() {
        return authority_id;
    }

    public void setAuthority_id(String authority_id) {
        this.authority_id = authority_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMailbox() {
        return mailbox;
    }

    public void setMailbox(String mailbox) {
        this.mailbox = mailbox;
    }

    public String getExtra_info_id() {
        return extra_info_id;
    }

    public void setExtra_info_id(String extra_info_id) {
        this.extra_info_id = extra_info_id;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "UserDo{" +
                "acc=" + acc +
                ", passwd='" + passwd + '\'' +
                ", authority_id='" + authority_id + '\'' +
                ", name='" + name + '\'' +
                ", mailbox='" + mailbox + '\'' +
                ", extra_info_id='" + extra_info_id + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }

}
