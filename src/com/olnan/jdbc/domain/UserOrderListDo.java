package com.olnan.jdbc.domain;

import java.math.BigInteger;
import java.util.Date;

/**
 * @ClassName : UserOrderListDo
 * @Author : ALVIN
 * @Date: 2021/1/22 14:44
 * @Description : 用户订单列表项数据对象类，传递向Dao
 */
public class UserOrderListDo {
    private BigInteger acc;
    private String ordr_id;
    private Date create_time;
    private Date update_time;

    public UserOrderListDo() {
    }

    public BigInteger getAcc() {
        return acc;
    }
    public void setAcc(BigInteger acc) {
        this.acc = acc;
    }
    public String getOrdr_id() {
        return ordr_id;
    }
    public void setOrdr_id(String ordr_id) {
        this.ordr_id = ordr_id;
    }
    public Date getCreate_time() {
        return create_time;
    }
    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }
    public Date getUpdate_time() {
        return update_time;
    }
    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "UserOrderListDo{" +
                "acc=" + acc +
                ", ordr_id='" + ordr_id + '\'' +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }
}
