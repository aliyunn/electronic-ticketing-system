package com.olnan.test;


import com.olnan.jdbc.dao.TicketTypeMapper;
import com.olnan.jdbc.dao.TicketTypeMatchMapper;
import com.olnan.jdbc.dao.UserMapper;
import com.olnan.jdbc.domain.UserDo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;


/**
 * @ClassName : MyBatisTest
 * @Author : ALVIN
 * @Date: 2021/1/23 16:39
 * @Description : MyBastis的测试类
 */
public class MyBatisTest{
    private Object UserMapper;

    @Test
    public void initTest() throws IOException {
        /**
         * 最初测试类，将过程走一遍
         */
//    全局配置文件
    String resource = "mybaties-config.xml";
    InputStream inputStream = Resources.getResourceAsStream(resource);
//    根据全局配置文件产生Sqlsession工厂SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
//    Sqlsession工厂产生session
    SqlSession openSession = sqlSessionFactory.openSession();
    UserDo user = openSession.selectOne("user.selectUser","1233244");
    System.out.println(user);
    System.out.println("账号:" + user.getAcc());
    System.out.println("密码:" + user.getPasswd());
    openSession.close();
    }


    @Test
    public void SecondTest() throws IOException {
        /**
         * 进化测试类，将过程走一遍
         */

        // mybatis配置文件
        String resource = "mybaties-config.xml";
        // 得到配置文件流
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 创建会话工厂，传入mybatis的配置文件信息
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);



        SqlSession sqlSession=sqlSessionFactory.openSession();
        //创建UserMapper对象，mybatis自动生成mapper代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        //调用UserMapper的方法
        BigInteger tem = new BigInteger(String.valueOf(1233245));
        UserDo user=userMapper.selectOneByAcc(tem);
        System.out.println(user);
        System.out.println("账号:" + user.getAcc());
        System.out.println("密码:" + user.getPasswd());

//        EticketMapper eticketMapper = sqlSession.getMapper(EticketMapper.class);
//        ETicketDo eTicketDo = (ETicketDo) eticketMapper.selectOneByOrdrId("1233");
//        System.out.println(eTicketDo);
        sqlSession.close();
    }
    private SqlSession openSession;
    @Before
    public void getSqlsession() throws IOException {
        // mybatis配置文件
        String resource = "mybaties-config.xml";
        // 得到配置文件流
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // 创建会话工厂，传入mybatis的配置文件信息
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //得到Sqlsession
        openSession = sqlSessionFactory.openSession();
    }
    @Test
    public void ZengShanTest() throws IOException {
        //创建UserMapper对象，mybatis自动生成mapper代理对象
        UserMapper userMapper = openSession.getMapper(UserMapper.class);
        TicketTypeMapper ticketTypeMapper = openSession.getMapper(TicketTypeMapper.class);
        TicketTypeMatchMapper ticketTypeMatchMapper = openSession.getMapper(TicketTypeMatchMapper.class);



        //查找某票种类实际的票的编号列表测试
//        try {
//            TicketTypeDo[] ticketTypeDos = ticketTypeMapper.selectListByTkttypId(new BigInteger(String.valueOf(1233253)));
//            for ( TicketTypeDo ticketTypeDo:ticketTypeDos ){
//                System.out.println("种类编号：" + ticketTypeDo.getTkt_id());
//            }
//            //手动提交
//            openSession.commit();
//        }finally {
//            openSession.close();
//        }


//        //删除票种编号匹配测试
//        try {
//            System.out.println("成功删除" + ticketTypeMapper.deleteOne(new BigInteger(String.valueOf(1233252)),"999999") + "项");
//            openSession.commit();
//        }finally {
//            openSession.close();
//        }


//        //增加票种编号匹配测试
//        try {
//            TicketTypeDo ticketTypeDo = new TicketTypeDo();
//            ticketTypeDo.setTkttyp_id(new BigInteger(String.valueOf(1233253)));
//            ticketTypeDo.setTkt_id("777777");
//            ticketTypeDo.setCreate_time(new Date(2021-01-06));
//            ticketTypeDo.setUpdate_time(new Date(2021-01-07));
//            int retint = ticketTypeMapper.insertOne(ticketTypeDo);
//            openSession.commit();
//            System.out.println("影响行数：" + retint);
//        }finally {
//            //注意使用完要关闭session
//            openSession.close();
//        }



        //增加用户测试
//        try {
//            UserDo user = new UserDo();
//            user.setAcc(null);
//            user.setPasswd("999999999");
//            user.setName("测试增加角色A");
//            user.setMailbox("shangran@aliyun.com");
//            user.setAuthority_id("1");
//            user.setExtra_info_id("0003");
//            user.setCreate_time(new Date(2021-01-06));
//            user.setUpdate_time(new Date(2021-01-07));
//            int retint = userMapper.insertOne(user);
//            openSession.commit();
//            System.out.println("影响行数：" + retint);
//            UserDo useadd = userMapper.selectOneByAcc(user.getAcc());
//            System.out.println("注册成功！您的新帐号为：" + useadd.getAcc());
//        }finally {
//            //注意使用完要关闭session
//            openSession.close();
//        }

        //删除用户测试
//        try {
//            System.out.println("成功删除" + userMapper.deleteOne(new BigInteger(String.valueOf(1233252))) + "项");
//            openSession.commit();
//        }finally {
//            openSession.close();
//        }

        //更改用户信息测试
//        try {
//            UserDo user = new UserDo();
//            user.setAcc(new BigInteger(String.valueOf(1233244)));
//            user.setPasswd("999999999");
//            user.setName("测试增加角色A");
//            user.setMailbox("shangran@aliyun.com");
//            user.setAuthority_id("1");
//            user.setExtra_info_id("0003");
//            user.setCreate_time(new Date(2021-01-06));
//            user.setUpdate_time(new Date(2021-01-07));
//            System.out.println("成功更新" + userMapper.updateOne(user) + "项");
//            openSession.commit();
//        }finally {
//            openSession.close();
//        }

        //查找一个用户测试
        try {
            System.out.println(userMapper.selectOneByAcc(new BigInteger(String.valueOf(1233244))));
            //手动提交
            openSession.commit();
        }finally {
            openSession.close();
        }
//        //查找某权限用户测试
//        try {
//            UserDo[] userlist = userMapper.selectListByAuthorityId("1");
//            for ( UserDo user:userlist ){
//                System.out.println("用户：" + user.getAcc());
//            }
//            //手动提交
//            openSession.commit();
//        }finally {
//            openSession.close();
//        }


    }
}
