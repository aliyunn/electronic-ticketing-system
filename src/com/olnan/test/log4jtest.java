package com.olnan.test;


import org.apache.log4j.Logger;

/**
 * @ClassName : log4jtest
 * @Author : ALVIN
 * @Date: 2021/1/23 14:19
 * @Description : 日志功能的测试类
 */
public class log4jtest {
    private static Logger logger = Logger.getLogger(log4jtest.class);
    public static void main(String args[]){
        System.out.println("这是测试类！");
        logger.info("this is info message！");
        logger.debug("this is debug message!");
        logger.error("this is an error!");
    }
}
